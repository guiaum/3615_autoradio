# rotary_encoder.py - read rotary encoder
# (c) BotBook.com - Karvinen, Karvinen, Valtokari
import time
import pygame
import botbook_gpio as gpio	# <1>

theSounds = ["/home/pi/3615_autoradio/wave/radio1.wav", "/home/pi/3615_autoradio/wave/radio2.wav", "/home/pi/3615_autoradio/wave/radio3.wav", "/home/pi/3615_autoradio/wave/radio4.wav", "/home/pi/3615_autoradio/wave/radio5.wav","/home/pi/3615_autoradio/wave/radio6.wav", "/home/pi/3615_autoradio/wave/radio8.wav", "/home/pi/3615_autoradio/wave/radio7.wav", "/home/pi/3615_autoradio/wave/radio9.wav", "/home/pi/3615_autoradio/wave/radio14.wav","/home/pi/3615_autoradio/wave/radio10.wav", "/home/pi/3615_autoradio/wave/radio15.wav", "/home/pi/3615_autoradio/wave/radio16.wav", "/home/pi/3615_autoradio/wave/radio11.wav", "/home/pi/3615_autoradio/wave/radio17.wav","/home/pi/3615_autoradio/wave/radio12.wav", "/home/pi/3615_autoradio/wave/radio18.wav", "/home/pi/3615_autoradio/wave/radio13.wav", "/home/pi/3615_autoradio/wave/radio19.wav"]
theChannels = []
encLim = 30.
trackLim = encLim/3.*2
noiseLim = encLim/3.
theDelay = 300
paused = False



def mapFromTo(x,a,b,c,d):
	y=(x-a)/(b-a)*(d-c)+c
	return y

def clip(val, min_, max_):
    return min_ if val < min_ else max_ if val > max_ else val


def changeTrack(channel, id):
	channel.stop()
	s = pygame.mixer.Sound(theSounds[id])
	channel.play(s, -1,0,0)
	


def main():

	global theChannels

	pygame.mixer.init(44100, -16, 2, 2048)
	pygame.mixer.set_num_channels(2)

	radioNoise = pygame.mixer.Sound("/home/pi/3615_autoradio/wave/radionoise.wav")
	channel0 = pygame.mixer.Channel(0)
	channel0.play(radioNoise, -1,0,0)
	channel0.set_volume(1)
        
	#for i in range (0,len(theSounds)):
	sound1 = pygame.mixer.Sound(theSounds[0])
	channel1 = pygame.mixer.Channel(1)
	channel1.play(sound1, -1,0,0)
	channel1.set_volume(0)
	

	encoderClk = 4
	encoderDt = 17
	gpio.mode(encoderClk, "in")
	gpio.mode(encoderDt, "in")
	encoderLast = gpio.LOW
	encoderPos = 0.
	lastEncoderPos = 0.
	# To store volumes
	noisevol = 100.
	trackvol = 0.
	# To store the ids
	leftid = 0
	rightid = 1
	maxtrack = len(theSounds)-1
	# Where we are 
	right = False
	mainID = 0
	playedID = 0

	lastMove = 0
	prevMove = 0

	indexFadeIn = 0.
	
	while True: 
		clk = gpio.read(encoderClk)
		if encoderLast == gpio.LOW and clk == gpio.HIGH:	# <2>
			if(gpio.read(encoderDt) == gpio.LOW):	# <3>
				encoderPos += 1
				lastMove = time.time()
			else:
				encoderPos -= 1
				lastMove = time.time()

		encoderLast = clk
		#if encoderPos != lastEncoderPos:
			


		lastEncoderPos = encoderPos
		prevMove = time.time()

		difference = prevMove-lastMove
		#print(difference)
		if (difference)>theDelay:
			#print("delai expire")
			paused = True
		else : 
			paused = False

		prevMove = lastMove 
		

		if encoderPos == encLim+1.:
			encoderPos = -encLim
			# Shift tracks
			if rightid < maxtrack:
				rightid+=1
			else: 
				rightid = 0

			if leftid < maxtrack:
				leftid+=1
			else: 
				leftid = 0

		elif encoderPos == -encLim-1.:
			encoderPos = encLim
			# Shift tracks

			if leftid > 0:
				leftid-=1
			else: 
				leftid = maxtrack

			if rightid > 0:
				rightid-=1
			else: 
				rightid = maxtrack


		# NOISE VOLUME
		if 	encoderPos > noiseLim:
			noisevol = mapFromTo(encoderPos,noiseLim,trackLim,100.,0.)
			
		else:
			noisevol = mapFromTo(encoderPos,-trackLim,-noiseLim,0.,100.)

		# TRACK VOLUME
		if 	encoderPos > noiseLim:
			trackvol = mapFromTo(encoderPos,noiseLim,trackLim,0,100)
		else:
			trackvol = mapFromTo(encoderPos,-trackLim,-noiseLim,100,0)

		# WICH TRACK DO WE LISTEN TO
		if 	encoderPos > 0:
			right = True
		else:
			right = False

		if right == True:
			mainID = rightid
		else:
			mainID = leftid

		noisevol = clip(noisevol, 0 , 100)
		trackvol = clip(trackvol, 0 ,100)

					
		if      trackvol == 0 and mainID != playedID:
		        changeTrack(channel1, mainID)
			print("Change Track")
			playedID=mainID

		if paused == True:
			noisevol = 0
			trackvol = 0
			indexFadeIn = 0
		else:
			noisevol = (noisevol/100)*indexFadeIn
			trackvol = (trackvol/100)*indexFadeIn
			if indexFadeIn<100:
				indexFadeIn = indexFadeIn + 1
		channel0.set_volume(noisevol/100.)
		channel1.set_volume(trackvol/100.)

		print("Encoder position %d" % encoderPos, "Noise Vol %d" % noisevol, "Track Vol %d" % trackvol, "LeftID %d" % leftid, "RightID %d" % rightid, "mainID %d" % mainID, "playedID %d" % playedID )	

		time.sleep(0.001) 


if __name__ == "__main__":
	main()
