# 3615_AUTORADIO

![pic1](3615_autoradio.jpg)

Code qui simule l'utilisation d'une radio, le volume du bruit est modulé à l'approche de l'une des pistes préparées.
Utilisation des channels de pygame, dans une Raspbian. Pi Zero W et JustBoom Amp Zero. 

Destiné à être intégré dans un vieil autoradio, pour la scénographie de l'exposition "On my radio" à Besançon.

Basé sur https://github.com/hackabletype/37-Sensors-Code/tree/master/raspberrypi/rotary_encoder
Utilisation de https://www.sparkfun.com/products/10596

Branchement de l'encodeur rotatif = pin physique 7 et 11 (BCM 4 et 17) + GND 
Côté bouton, pin du milieu = GND, les deux autres CLK et Dt

Il est nécessaire de faire un pullup sur BCM 17 (https://www.raspberrypi.org/forums/viewtopic.php?f=91&t=131032)
